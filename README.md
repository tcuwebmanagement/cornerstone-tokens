# Cornerstone Tokens

This is a central repository for Cornerstone tokens.

# Installation

## Install via NPM

`npm install bitbucket:tcuwebmanagement/cornerstone_tokens`

# Including Cornerstone Tokens in a Project

The json files are formatted in a way that [Theo](https://github.com/salesforce-ux/theo)
by Salesforce supports. Theo ships with predefined formats but you can also
setup your own [custom format](https://github.com/salesforce-ux/theo#custom-format-handlebars).

For Cornerstone projects, we use gulp and a custom format to output scss files
with variables for each token.

Example gulp task:
```
/**
 * Transform token json files to scss files
 */
const theoConfig = require('./theo.config');
function tokens() {
	// Transform tokens/props.json to scss/props.scss:
	return src(`node_modules/cornerstone_tokens/core/*.json`)
		.pipe(
			gulpTheo({
				transform: { type: 'web' },
				format: { type: 'scss' },
			})
		)
		.pipe(dest('assets/scss/tokens'));
}
```

Example theo.config.js:
```
const path = require('path');
const theo = require('theo');

('use strict');

const componentPath = path.resolve(__dirname, 'src');

const template = `// This file has been dynamically generated from design tokens
// Source: {{relative "${componentPath}" meta.file}}

{{#each props as |prop|}}
{{#if prop.comment}}
{{{trimLeft (indent (comment (trim prop.comment)))}}}
{{/if}}
\${{dashcase prop.name}}: {{#eq prop.type "string"}}"{{/eq}}{{{prop.value}}}{{#eq prop.type "string"}}"{{/eq}} !default;
{{/each}}
`;

theo.registerFormat('scss', `${template}`);
```