# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.0.0] - 2024-03-15

### Changed

- Sync up all tokens

## [2.0.0] - 2023-11-10

### Added

- Harris College tokens

### Changed

- Token naming scheme

## [1.1.0] - 2023-09-25

### Added

- Honors tokens